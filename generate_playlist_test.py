from unittest import TestCase
from client.playlist_generation import *


class TestPlaylistGeneration(TestCase):
    
    def test_n_chanson_by_artiste(self):
        ## GIVEN
        RUDY = [{
        "artiste": "daft punk",
        "note": 18
        },
        {
            "artiste":"gloria gaynor",
            "note": 10
        },
        {
            "artiste":"boney m",
            "note": 5
        },
        {
            "artiste":"oasis",
            "note": 20
        },
        {
            "artiste":"Bob Marley",
            "note": 10
        },
        {
            "artiste":"Tryo",
            "note": 20
        },
        {
            "artiste":"Technotronic",
            "note": 10
        },
        {
            "artiste":"dire straits",
            "note": 16
        }]
        
        n_song = [k for k in range(1,21)]
        n_song_generated =[]
        ## WHEN
        
        for k in n_song :
            n_song_generated.append(sum([elt["n_song"] for elt in nb_chanson_to_generate_by_artist(RUDY,k)]))

        ## THEN
        self.assertEqual(n_song,n_song_generated)
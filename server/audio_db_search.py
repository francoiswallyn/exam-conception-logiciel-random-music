import requests
import os
from dotenv import load_dotenv
import random
load_dotenv()
AUDIODB = os.environ.get("AUDIO_DB_ENDPOINT")
SEARCH_ARTIST = "search.php?s="
SEARCH_ALBUM = "album.php?i="
SEARCH_TRACK = "track.php?m="

def get_artist_id(artist_name:str):
    """
    Renvoie l'ID d'artiste en fonction du nom d'artiste. 

    Args:
        artist_name (str): Nom de l'artiste

    Returns:
        str: ID de l'artiste sur l'API AudioDB, 
            False si l'API ne répond pas
    """
    response_artist = requests.get(AUDIODB + SEARCH_ARTIST + artist_name)
    if response_artist.status_code == 200 :
        response_artist_dict =  response_artist.json()
        if response_artist_dict["artists"] == None :
            return None
        return response_artist_dict["artists"][0]["idArtist"]
    else : 
        return False

def get_random_album_id(artist_id:str):
    """
    Renvoie un ID d'album de façon aléatoire à partir del'ID
    d'un artiste.

    Args:
        artist_id (str): ID de l'artiste

    Returns:
        str: ID d'un album
            False si l'API ne répond pas
    """
    
    response_album = requests.get(AUDIODB + SEARCH_ALBUM + artist_id)    
    if response_album.status_code == 200 :
        response_album_dict = response_album.json()
        all_albums = response_album_dict["album"]
        album = random.choice(all_albums)
        return album["idAlbum"]
    else :
        return False

def get_random_track(album_id:str):
    """
    Renvoie un ID de chanson à partir d'un ID d'album

    Args:
        album_id (str): id d'album
    """
    
    response_track = requests.get(AUDIODB + SEARCH_TRACK + album_id)
    if response_track.status_code == 200 :
        response_track_dict = response_track.json()
        all_tracks = response_track_dict["track"]
        if len(all_tracks)>0:
            track = random.choice(all_tracks)
            strArtist = track["strArtist"]
            title = track["strTrack"]
            suggested_youtube_url = track["strMusicVid"]
            return {"artist" : strArtist, "title" : title, "suggested_youtube_url" : suggested_youtube_url}
        else:
            return None
    else :
        return False
    
def get_random_track_from_artist_name(artist_name:str):
    artist_id = get_artist_id(artist_name)
    if artist_id :
        album_id = get_random_album_id(artist_id)
        if album_id :
            track = get_random_track(album_id)
            if track :
                return track
            else : 
                return False
        else : 
            return False
    else : 
        return False
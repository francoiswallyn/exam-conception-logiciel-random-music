import requests
import os
import json
from dotenv import load_dotenv
import random
load_dotenv()
LYRYCSOVH = os.environ.get("LYRICS_OVH_ENDPOINT")

def get_lyrics(artist_name :str,track_name : str):
    response_lyrics = requests.get(LYRYCSOVH + artist_name + "/" + track_name)
    if response_lyrics.status_code == 200:
        response_lyrics_dict = response_lyrics.json()
        if response_lyrics_dict == {"error":"No lyrics found"}:
            lyrics =  "No lyrics found"
        else : 
            return response_lyrics_dict["lyrics"]
    else :
        return False

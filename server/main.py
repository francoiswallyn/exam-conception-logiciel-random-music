from fastapi import FastAPI
import requests
import json
import uvicorn
from dotenv import load_dotenv
import os
from audio_db_search import get_random_track_from_artist_name
from lyrics_ovh_search import get_lyrics


## Loading environment variables
load_dotenv()
AUDIODB = os.environ.get("AUDIO_DB_ENDPOINT")
LYRICS_OVH = os.environ.get("LYRICS_OVH_ENDPOINT")
SEARCH_ARTIST = "search.php?s="
SEARCH_ALBUM = "album.php?i="
SEARCH_TRACK = "track.php?m="

app = FastAPI()

@app.get("/")
def test_apis():
    """
    Test du bon fonctionnement des API. 

    Returns:
        _type_: _description_
    """
    #request AudioDB
    requestAudioDB = requests.get(AUDIODB + SEARCH_ARTIST + "rick astley")
    #request Lyrics ovh
    requestLyrics = requests.get(LYRICS_OVH + "Rick Astley" + "/" + "Never Gonna Give You Up")
    #get status codes
    status_code_audio_db = requestAudioDB.status_code
    status_code_lyrics = requestLyrics.status_code
    return {"AudioDB_status_code":status_code_audio_db,"LyricsOVH_status_code": status_code_lyrics}

@app.get("/random/{artist_name}")
def get_random_music(artist_name:str):
    track_info = get_random_track_from_artist_name(artist_name)
    if track_info :
        lyrics = get_lyrics(track_info["artist"],track_info["title"])
        if lyrics :
            track_info["lyrics"]=lyrics
            return track_info
        else : 
            track_info["lyrics"] ="No lyrics available"
            return track_info
    else :
          return {"artist" : artist_name, "error": "No informations available"} 


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, log_level="info")
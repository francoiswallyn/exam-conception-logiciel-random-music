from playlist_generation import *

#PATH_TO_RUDY_FILE = "/Users/francoiswallyn/Desktop/ENSAI/2A/S2/conception_logiciel/rudy.json"
#PATH_TO_IMPORT = "/Users/francoiswallyn/Desktop/ENSAI/2A/S2/conception_logiciel"
#FILE_NAME_TO_IMPORT = "playlist_rudy"
#NB_TOT_SONG = 20


def main():
    if test_api_random_music():
        path_to_rudy_file = input("Chemin vers le fichier .json des artistes (Q pour quitter) ")
        if path_to_rudy_file == "Q":
                print("Bye")
        else :
            rudy_file = load_json(path_to_rudy_file)
            if rudy_file :
                nb_tot_song = int(input("Nombre de musique à générer (entier positif): "))
                rudy_n_song = nb_chanson_to_generate_by_artist(rudy_file,nb_tot_song)     
                playlist = generate_playlist(rudy_n_song)
                path_to_import = input("Chemin vers le dossier pour enregistrer la playlist : ")
                file_name_to_import = input("Nom de la playlist : ")
                to_json(playlist,path_to_import,file_name_to_import)
            else :
                print("Le nom de fichier est invalide veuillez rééssayer")
                main()
            
    else :
        print("L'API Random Music est indisponible, rééssayer plus tard")

if __name__ == "__main__":
    main()
    
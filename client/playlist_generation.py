import requests
import json
from copy import deepcopy
from random import choices, shuffle 

RANDOM_MUSIC_ENDPOINT = "http://127.0.0.1:8000/"

def load_json(path_to_rudy_file : str):
    try :
        with open(path_to_rudy_file) as json_file:
            data = json.load(json_file)
        return data
    except FileNotFoundError :
        return False

def to_json(file:list,path_to_file:str,file_name:str):
    with open(f"{path_to_file}/{file_name}.json",mode="w") as outfile:
        json.dump(file, outfile)
        
def test_api_random_music():
    """
    Teste la disponibilité de l'API random music, de l'API AudioDB
    et de l'API LyricsOVH

    Returns:
        _type_: _description_
    """
    try :
        resp = requests.get(RANDOM_MUSIC_ENDPOINT)
        if resp.status_code ==200 :
            resp_dict = resp.json()
            if resp_dict["AudioDB_status_code"]==200 and resp_dict["LyricsOVH_status_code"]==200:
                return True
            return False
        return False
    except requests.exceptions.RequestException as e:
        return False

def nb_chanson_to_generate_by_artist(rudy_file:list,nb_song_tot:int):
    """
    Définit le nombre de chansons à générer par artiste
    en fonction des notes.

    Args:
        rudy_file (list): rudy file

    Returns:
        list[dict]: liste d'artistes avec le nombre de chansons
                    à générer
    """
    ## Total des notes
    tot_notes = sum([info["note"] for info in rudy_file])
    
    ## Calul du nombre de chanson par artiste
    nb_chanson_to_generate_by_artist = [{"artist" : info["artiste"],
                                         "n_song" :round((info["note"]/tot_notes)*nb_song_tot)} for info in rudy_file]
    
    ## Nombre de total de chanson à générer calculé
    s = sum([elt["n_song"] for elt in nb_chanson_to_generate_by_artist])
    diff = nb_song_tot - s
    ## si c'est différent de du nombre demandé
    if diff < 0 :
        indexes = [k for k in range(len(nb_chanson_to_generate_by_artist))]
        weights = [(tot_notes-elt["note"]) for elt in rudy_file]
        while diff <0 :
            i = choices(indexes,weights,k=1)[0]
            while nb_chanson_to_generate_by_artist[i]["n_song"]<=0:
                weights.pop(i)
                indexes.remove(i)
                i = choices(indexes,weights,k=1)[0]
            nb_chanson_to_generate_by_artist[i]["n_song"] -= 1
            diff+=1
    elif diff > 0: 
        while diff >0 :
            indexes = [k for k in range(len(nb_chanson_to_generate_by_artist))]
            weights = [elt["note"] for elt in rudy_file]
            i = choices(indexes,weights,k=1)[0]
            nb_chanson_to_generate_by_artist[i]["n_song"] += 1
            diff-=1
    return nb_chanson_to_generate_by_artist
    
def generate_n_song_from_artist(artist_name:str,nb_song:int):
    """
    Génère le nombre de sons aléatoires d'un artiste 
    Args:
        artist_name (str): nom d'artiste
        nb_song (int): nombre de sons à génerer pour l'artiste
    """
    track_list = []
    for i in range(nb_song):
        resp_track = requests.get(RANDOM_MUSIC_ENDPOINT+"random/"+artist_name)
        track = resp_track.json()
        while track["title"] in [t["title"]  for t in track_list if t] :
            resp_track = requests.get(RANDOM_MUSIC_ENDPOINT+"random/"+artist_name)
            track = resp_track.json()
        track_list.append(deepcopy(track))
    return track_list

def generate_playlist(artist_nb_song : list):
    """
    génère une playlist

    Args:
        artist_nb_song (list): liste de dictionnaires nom d'artiste : nombre de sons

    Returns:
        list[dict]: listes de dictionnaires donnant les informations sur les musiques
    """
    playlist = []
    for elt in artist_nb_song:
        playlist += generate_n_song_from_artist(elt["artist"],elt["n_song"])
    shuffle(playlist)
    return playlist
